package io.foodmart.dto;

public class ProductDto {
	
	  int product_class_id = 0;
	  int productId = 0;	  
	  String product_subcategory = null;
	  String product_category = null;
	  String product_department = null;
	  String product_family = null;
	  String brand_name = null;
	  String product_name = null;
	  long SKU = 0;
	  double SKP = 0.0;
	  double gross_weight = 0.0;
	  double net_weight = 0.0;
	  boolean recyclable_package = false;
	  boolean low_fat = false;
	  short units_per_case = 0;
	  short cases_per_pallet = 0;
	  double shelf_width = 0.0;
	  double shelf_height = 0.0;
	  double shelf_depth = 0.0;

	  public ProductDto(
		      int product_class_id,
		      int productId,
		      String product_subcategory,
		      String product_category,
		      String product_department,
		      String product_family,
		      String brand_name,
		      String product_name,
		      long sKU,
		      double sKP,
		      double gross_weight,
		      double net_weight,
		      boolean recyclable_package,
		      boolean low_fat,
		      short units_per_case,
		      short cases_per_pallet,
		      double shelf_width,
		      double shelf_height,
		      double shelf_depth) {
		    super();
		    this.product_class_id = product_class_id;
		    this.productId = productId;
		    this.product_subcategory = product_subcategory;
		    this.product_category = product_category;
		    this.product_department = product_department;
		    this.product_family = product_family;
		    this.brand_name = brand_name;
		    this.product_name = product_name;
		    SKU = sKU;
		    SKP = sKP;
		    this.gross_weight = gross_weight;
		    this.net_weight = net_weight;
		    this.recyclable_package = recyclable_package;
		    this.low_fat = low_fat;
		    this.units_per_case = units_per_case;
		    this.cases_per_pallet = cases_per_pallet;
		    this.shelf_width = shelf_width;
		    this.shelf_height = shelf_height;
		    this.shelf_depth = shelf_depth;
		  }
	  

	  public ProductDto() {
    // TODO Auto-generated constructor stub
  }


	  public int getProduct_class_id() {
	    return product_class_id;
	  }
	
	  public void setProduct_class_id(int product_class_id) {
	    this.product_class_id = product_class_id;
	  }
	
	  public int getProductId() {
	    return productId;
	  }
	
	  public void setProductId(int productId) {
	    this.productId = productId;
	  }
	
	  public String getProduct_subcategory() {
	    return product_subcategory;
	  }
	
	  public void setProduct_subcategory(String product_subcategory) {
	    this.product_subcategory = product_subcategory;
	  }
	
	  public String getProduct_category() {
	    return product_category;
	  }
	
	  public void setProduct_category(String product_category) {
	    this.product_category = product_category;
	  }
	
	  public String getProduct_department() {
	    return product_department;
	  }
	
	  public void setProduct_department(String product_department) {
	    this.product_department = product_department;
	  }
	
	  public String getProduct_family() {
	    return product_family;
	  }
	
	  public void setProduct_family(String product_family) {
	    this.product_family = product_family;
	  }
	
	  public String getBrand_name() {
	    return brand_name;
	  }
	
	  public void setBrand_name(String brand_name) {
	    this.brand_name = brand_name;
	  }
	
	  public String getProduct_name() {
	    return product_name;
	  }
	
	  public void setProduct_name(String product_name) {
	    this.product_name = product_name;
	  }
	
	  public long getSKU() {
	    return SKU;
	  }
	
	  public void setSKU(long sKU) {
	    SKU = sKU;
	  }
	
	  public double getSKP() {
	    return SKP;
	  }
	
	  public void setSKP(double sKP) {
	    SKP = sKP;
	  }
	
	  public double getGross_weight() {
	    return gross_weight;
	  }
	
	  public void setGross_weight(double gross_weight) {
	    this.gross_weight = gross_weight;
	  }
	
	  public double getNet_weight() {
	    return net_weight;
	  }
	
	  public void setNet_weight(double net_weight) {
	    this.net_weight = net_weight;
	  }
	
	  public boolean isRecyclable_package() {
	    return recyclable_package;
	  }
	
	  public void setRecyclable_package(boolean recyclable_package) {
	    this.recyclable_package = recyclable_package;
	  }
	
	  public boolean isLow_fat() {
	    return low_fat;
	  }
	
	  public void setLow_fat(boolean low_fat) {
	    this.low_fat = low_fat;
	  }
	
	  public short getUnits_per_case() {
	    return units_per_case;
	  }
	
	  public void setUnits_per_case(short units_per_case) {
	    this.units_per_case = units_per_case;
	  }
	
	  public short getCases_per_pallet() {
	    return cases_per_pallet;
	  }
	
	  public void setCases_per_pallet(short cases_per_pallet) {
	    this.cases_per_pallet = cases_per_pallet;
	  }
	
	  public double getShelf_width() {
	    return shelf_width;
	  }
	
	  public void setShelf_width(double shelf_width) {
	    this.shelf_width = shelf_width;
	  }
	
	  public double getShelf_height() {
	    return shelf_height;
	  }
	
	  public void setShelf_height(double shelf_height) {
	    this.shelf_height = shelf_height;
	  }
	
	  public double getShelf_depth() {
	    return shelf_depth;
	  }
	
	  public void setShelf_depth(double shelf_depth) {
	    this.shelf_depth = shelf_depth;
	  }
}
