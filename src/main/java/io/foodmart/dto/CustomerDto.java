package io.foodmart.dto;

public class CustomerDto {
	  int customer_id = 0;
	  double account_num = 0.0D;
	  String lname = null;
	  String fname = null;
	  String mi = null;
	  String address1 = null;
	  String address2 = null;
	  String address3 = null;	  
	  String address4 = null;	  
	  String city = null;
	  String state_province = null;
	  String postal_code = null;
	  String country = null;
	  String customer_region_id = null;
	  String phone1 = null;
	  String phone2 = null;
	  String birthdate = null;
	  String marital_status = null;
	  String yearly_income = null;
	  String gender = null;
	  short total_children = 0;
	  short num_children_at_home = 0;
	  String education = null;
	  String date_accnt_opened = null;
	  String member_card = null;
	  String occupation = null;
	  String houseowner = null;
	  int num_cars_owned = 0;
	  String fullname = null;
	  
	  public int getCustomer_id() {
	    return customer_id; }
	  
	  public void setCustomer_id(int customer_id)
	  {
	    this.customer_id = customer_id;
	  }
	  
	  public double getAccount_num() {
	    return account_num;
	  }
	  
	  public void setAccount_num(double account_num) {
	    this.account_num = account_num;
	  }
	  
	  public String getLname() {
	    return lname;
	  }
	  
	  public void setLname(String lname) {
	    this.lname = lname;
	  }
	  
	  public String getFname() {
	    return fname;
	  }
	  
	  public void setFname(String fname) {
	    this.fname = fname;
	  }
	  
	  public String getMi() {
	    return mi;
	  }
	  
	  public void setMi(String mi) {
	    this.mi = mi;
	  }
	  
	  public String getAddress1() {
	    return address1;
	  }
	  
	  public void setAddress1(String address1) {
	    this.address1 = address1;
	  }
	  
	  public String getAddress2() {
	    return address2;
	  }
	  
	  public void setAddress2(String address2) {
	    this.address2 = address2;
	  }
	  
	  public String getAddress3() {
	    return address3;
	  }
	  
	  public void setAddress3(String address3) {
	    this.address3 = address3;
	  }
	  
	  public String getAddress4() {
	    return address4;
	  }
	  
	  public void setAddress4(String address4) {
	    this.address4 = address4;
	  }
	  
	  public String getCity() {
	    return city;
	  }
	  
	  public void setCity(String city) {
	    this.city = city;
	  }
	  
	  public String getState_province() {
	    return state_province;
	  }
	  
	  public void setState_province(String state_province) {
	    this.state_province = state_province;
	  }
	  
	  public String getPostal_code() {
	    return postal_code;
	  }
	  
	  public void setPostal_code(String postal_code) {
	    this.postal_code = postal_code;
	  }
	  
	  public String getCountry() {
	    return country;
	  }
	  
	  public void setCountry(String country) {
	    this.country = country;
	  }
	  
	  public String getCustomer_region_id() {
	    return customer_region_id;
	  }
	  
	  public void setCustomer_region_id(String customer_region_id) {
	    this.customer_region_id = customer_region_id;
	  }
	  
	  public String getPhone1() {
	    return phone1;
	  }
	  
	  public void setPhone1(String phone1) {
	    this.phone1 = phone1;
	  }
	  
	  public String getPhone2() {
	    return phone2;
	  }
	  
	  public void setPhone2(String phone2) {
	    this.phone2 = phone2;
	  }
	  
	  public String getBirthdate() {
	    return birthdate;
	  }
	  
	  public void setBirthdate(String birthdate) {
	    this.birthdate = birthdate;
	  }
	  
	  public String getMarital_status() {
	    return marital_status;
	  }
	  
	  public void setMarital_status(String marital_status) {
	    this.marital_status = marital_status;
	  }
	  
	  public String getYearly_income() {
	    return yearly_income;
	  }
	  
	  public void setYearly_income(String yearly_income) {
	    this.yearly_income = yearly_income;
	  }
	  
	  public String getGender() {
	    return gender;
	  }
	  
	  public void setGender(String gender) {
	    this.gender = gender;
	  }
	  
	  public short getTotal_children() {
	    return total_children;
	  }
	  
	  public void setTotal_children(short total_children) {
	    this.total_children = total_children;
	  }
	  
	  public short getNum_children_at_home() {
	    return num_children_at_home;
	  }
	  
	  public void setNum_children_at_home(short num_children_at_home) {
	    this.num_children_at_home = num_children_at_home;
	  }
	  
	  public String getEducation() {
	    return education;
	  }
	  
	  public void setEducation(String education) {
	    this.education = education;
	  }
	  
	  public String getDate_accnt_opened() {
	    return date_accnt_opened;
	  }
	  
	  public void setDate_accnt_opened(String date_accnt_opened) {
	    this.date_accnt_opened = date_accnt_opened;
	  }
	  
	  public String getMember_card() {
	    return member_card;
	  }
	  
	  public void setMember_card(String member_card) {
	    this.member_card = member_card;
	  }
	  
	  public String getOccupation() {
	    return occupation;
	  }
	  
	  public void setOccupation(String occupation) {
	    this.occupation = occupation;
	  }
	  
	  public String getHouseowner() {
	    return houseowner;
	  }
	  
	  public void setHouseowner(String houseowner) {
	    this.houseowner = houseowner;
	  }
	  
	  public int getNum_cars_owned() {
	    return num_cars_owned;
	  }
	  
	  public void setNum_cars_owned(int num_cars_owned) {
	    this.num_cars_owned = num_cars_owned;
	  }
	  
	  public String getFullname() {
	    return fullname;
	  }
	  
	  public void setFullname(String fullname) {
	    this.fullname = fullname;
	  }
}
