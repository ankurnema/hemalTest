package io.foodmart.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodmartfinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodmartfinalApplication.class, args);
	}
}
