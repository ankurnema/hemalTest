package io.foodmart.app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;

import io.foodmart.dto.ProductDto;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import io.foodmart.dto.CustomerDto;

@RestController
public class DataController {
	private static final String PRODUCT_CSV_FILE = "src/main/resources/product.csv";
	private static final String CUSTOMER_CSV_FILE = "src/main/resources/customer.csv";
	private CSVPrinter csvPrinter;
	  
	@RequestMapping(value = "/product", params = {
    "output=json"
	})
  @ResponseBody
	String getProductJson(@RequestParam("output") String output) {
			String resultInJson = null;
			 try {
			      List<ProductDto> ProductDtoList = new ArrayList<ProductDto>();
			      Connection connection = DriverManager.getConnection("jdbc:hsqldb:res:foodmart");
			      Statement statement = connection.createStatement();
			      ResultSet resultSet = statement.executeQuery("SELECT \"product_class_id\", \"product_id\", \"product_subcategory\", \"product_category\", \"product_department\", \"product_family\", \"brand_name\", \"product_name\", SKU, SRP, \"gross_weight\", \"net_weight\", \"recyclable_package\", \"low_fat\", \"units_per_case\", \"cases_per_pallet\", \"shelf_width\", \"shelf_height\", \"shelf_depth\" FROM \"foodmart\".\"product_class\" JOIN \"foodmart\".\"product\" ON \"product_class_id\" = \"product_class_id\" LIMIT 15");
			      while (resultSet.next()) {
			        ProductDto ProductDto = new ProductDto();
			       
			        ProductDto.setProduct_class_id(resultSet.getInt(1));  			        
			        ProductDto.setProductId(resultSet.getInt(2)); 
			        ProductDto.setProduct_subcategory(resultSet.getString(3));
			        ProductDto.setProduct_category(resultSet.getString(4));
			        ProductDto.setProduct_department(resultSet.getString(5));
			        ProductDto.setProduct_family(resultSet.getString(6));
			        ProductDto.setBrand_name(resultSet.getString(7));
			        ProductDto.setProduct_name(resultSet.getString(8));
			        ProductDto.setSKU(resultSet.getLong(9));
			        ProductDto.setSKP(resultSet.getDouble(10));
			        ProductDto.setGross_weight(resultSet.getDouble(11));
			        ProductDto.setNet_weight(resultSet.getDouble(12));
			        ProductDto.setRecyclable_package(resultSet.getBoolean(13));
			        ProductDto.setLow_fat(resultSet.getBoolean(14));
			        ProductDto.setUnits_per_case(resultSet.getShort(15));
			        ProductDto.setCases_per_pallet(resultSet.getShort(16));
			        ProductDto.setShelf_width(resultSet.getDouble(17));
			        ProductDto.setShelf_height(resultSet.getDouble(18));
			        ProductDto.setShelf_depth(resultSet.getDouble(19));
			        
			        ProductDtoList.add(ProductDto);
			      }
			      resultSet.close();
			      statement.close();
			      connection.close();
			      Gson gson = new Gson();
			      resultInJson = gson.toJson(ProductDtoList);
			      
			      
			      return resultInJson;
			    } catch (SQLException e) {
			      e.printStackTrace();
			      return "SQLException in class DataController : " + e.getMessage();
			    }
	}
	
    @RequestMapping(value = "/product", params = {
	        "output=csv"
	})
	    String geProductCsv(@RequestParam("output") String output) throws IOException {
  			 try {
  			      
	  			  BufferedWriter writer = Files.newBufferedWriter(Paths.get(PRODUCT_CSV_FILE));

	  	          csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("product_class_id", "product_id", "product_subcategory", "product_category", "product_department", "product_family", "brand_name", "product_name", "SKU", "SRP", "gross_weight", "net_weight", "recyclable_package", "low_fat", "units_per_case", "cases_per_pallet", "shelf_width", "shelf_height", "shelf_depth"));	  	          
  			      Connection connection = DriverManager.getConnection("jdbc:hsqldb:res:foodmart");
  			      Statement statement = connection.createStatement();
  			      ResultSet resultSet = statement.executeQuery("SELECT \"product_class_id\", \"product_id\", \"product_subcategory\", \"product_category\", \"product_department\", \"product_family\", \"brand_name\", \"product_name\", SKU, SRP, \"gross_weight\", \"net_weight\", \"recyclable_package\", \"low_fat\", \"units_per_case\", \"cases_per_pallet\", \"shelf_width\", \"shelf_height\", \"shelf_depth\" FROM \"foodmart\".\"product_class\" JOIN \"foodmart\".\"product\" ON \"product_class_id\" = \"product_class_id\" LIMIT 15");
  			      List<ProductDto> ProductDtoList = new ArrayList<ProductDto>();
  			      while (resultSet.next()) {
  			    	  csvPrinter.printRecord(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getLong(9), resultSet.getDouble(10), resultSet.getDouble(11), resultSet.getDouble(12), resultSet.getBoolean(13), resultSet.getBoolean(14), resultSet.getShort(15), resultSet.getShort(16), resultSet.getDouble(17), resultSet.getDouble(18),resultSet.getDouble(19));
  			    	ProductDto ProductDto = new ProductDto();
	  			       
  			    	ProductDto.setProduct_class_id(resultSet.getInt(1));  			        
			        ProductDto.setProductId(resultSet.getInt(2)); 
			        ProductDto.setProduct_subcategory(resultSet.getString(3));
			        ProductDto.setProduct_category(resultSet.getString(4));
			        ProductDto.setProduct_department(resultSet.getString(5));
			        ProductDto.setProduct_family(resultSet.getString(6));
			        ProductDto.setBrand_name(resultSet.getString(7));
			        ProductDto.setProduct_name(resultSet.getString(8));
			        ProductDto.setSKU(resultSet.getLong(9));
			        ProductDto.setSKP(resultSet.getDouble(10));
			        ProductDto.setGross_weight(resultSet.getDouble(11));
			        ProductDto.setNet_weight(resultSet.getDouble(12));
			        ProductDto.setRecyclable_package(resultSet.getBoolean(13));
			        ProductDto.setLow_fat(resultSet.getBoolean(14));
			        ProductDto.setUnits_per_case(resultSet.getShort(15));
			        ProductDto.setCases_per_pallet(resultSet.getShort(16));
			        ProductDto.setShelf_width(resultSet.getDouble(17));
			        ProductDto.setShelf_height(resultSet.getDouble(18));
			        ProductDto.setShelf_depth(resultSet.getDouble(19));
  			        
  			        ProductDtoList.add(ProductDto);
  			      }
  			      resultSet.close();
  			      statement.close();
  			      connection.close();
  			      csvPrinter.flush();
  			      
  			    String line = "abc";
  			    BufferedReader br = new BufferedReader(new FileReader(PRODUCT_CSV_FILE));
  			    try {
  			        StringBuilder sb = new StringBuilder();
  			         line = br.readLine();

  			        while (line != null) {
  			            sb.append(line);
  			            sb.append("\n");
  			            line = br.readLine();
  			        }
  			        return sb.toString();
  			    } finally {
  			        br.close();
  			    }
  	
  			    } catch (SQLException e) {
  			      e.printStackTrace();
  			      return "SQLException in class DataController : " + e.getMessage();
  			    }
	    }
 

    
    @RequestMapping(value = "/customer", params = {
    	    "output=json"
    		})
    		String getCustomerJson(@RequestParam("output") String output) {
    				String resultInJson = null;
    				 try {
    				      List<CustomerDto> CustomerDtoList = new ArrayList<CustomerDto>();
    				      Connection connection = DriverManager.getConnection("jdbc:hsqldb:res:foodmart");
    				      Statement statement = connection.createStatement();
    				      ResultSet resultSet = statement.executeQuery("SELECT * FROM \"foodmart\".\"customer\" LIMIT 15");
    				      while (resultSet.next()) {
    				    	CustomerDto CustomerDto = new CustomerDto();
    				       
    				    	CustomerDto.setCustomer_id(resultSet.getInt(1));
    				    	CustomerDto.setAccount_num(resultSet.getDouble(2));
    				    	CustomerDto.setLname(resultSet.getString(3));
    				    	CustomerDto.setFname(resultSet.getString(4));
    				    	CustomerDto.setMi(resultSet.getString(5));
    				    	CustomerDto.setAddress1(resultSet.getString(6));
    				    	CustomerDto.setAddress2(resultSet.getString(7));
    				    	CustomerDto.setAddress3(resultSet.getString(8));
    				    	CustomerDto.setAddress4(resultSet.getString(9));
    				    	CustomerDto.setCity(resultSet.getString(10));
    				    	CustomerDto.setState_province(resultSet.getString(11));
    				    	CustomerDto.setPostal_code(resultSet.getString(12));
    				    	CustomerDto.setCountry(resultSet.getString(13));
    				    	CustomerDto.setCustomer_region_id(resultSet.getString(14));
    				    	CustomerDto.setPhone1(resultSet.getString(15));
    				    	CustomerDto.setPhone2(resultSet.getString(16));
    				    	CustomerDto.setBirthdate(resultSet.getString(17));
    				    	CustomerDto.setMarital_status(resultSet.getString(18));
    				    	CustomerDto.setYearly_income(resultSet.getString(19));
    				    	CustomerDto.setGender(resultSet.getString(20));
    				    	CustomerDto.setTotal_children(resultSet.getShort(21));
    				    	CustomerDto.setNum_children_at_home(resultSet.getShort(22));
    				    	CustomerDto.setEducation(resultSet.getString(23));
    				    	CustomerDto.setDate_accnt_opened(resultSet.getString(24));
    				    	CustomerDto.setMember_card(resultSet.getString(25));
    				    	CustomerDto.setOccupation(resultSet.getString(26));
    				    	CustomerDto.setHouseowner(resultSet.getString(27));
    				    	CustomerDto.setNum_cars_owned(resultSet.getInt(28));
    				    	CustomerDto.setFullname(resultSet.getString(29));
    				    	
    				        CustomerDtoList.add(CustomerDto);
    				        
    				      }
    				      resultSet.close();
    				      statement.close();
    				      connection.close();
    				      Gson gson = new Gson();
    				      resultInJson = gson.toJson(CustomerDtoList);
    				      
    				      
    				      return resultInJson;
    				    } catch (SQLException e) {
    				      e.printStackTrace();
    				      return "SQLException in class DataController : " + e.getMessage();
    				    }
    		}
    		
    	    @RequestMapping(value = "/customer", params = {
    		        "output=csv"
    		})
    		    String getCustomerCsv(@RequestParam("output") String output) throws IOException {
    	  			 try {
    	  			      
    		  			  BufferedWriter writer = Files.newBufferedWriter(Paths.get(CUSTOMER_CSV_FILE));

    		  	          csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("customer_id", "account_num", "lname", "fname", "mi", "address1", "address2", "address3", "address4", "city", "state_province", "postal_code", "country", "customer_region_id", "phone1", "phone2", "birthdate", "marital_status", "yearly_income", "gender", "total_children", "num_children_at_home", "education", "date_accnt_opened", "member_card", "occupation", "houseowner", "num_cars_owned", "fullname"));	  	          
    	  			      Connection connection = DriverManager.getConnection("jdbc:hsqldb:res:foodmart");
    	  			      Statement statement = connection.createStatement();
    	  			    ResultSet resultSet = statement.executeQuery("SELECT * FROM \"foodmart\".\"customer\" LIMIT 15");
    	  			      List<CustomerDto> CustomerDtoList = new ArrayList<CustomerDto>();
    	  			      while (resultSet.next()) {
    	  			    	  csvPrinter.printRecord(resultSet.getInt(1), resultSet.getDouble(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10), resultSet.getString(11), resultSet.getString(12), resultSet.getString(13), resultSet.getString(14), resultSet.getString(15), resultSet.getString(16), resultSet.getString(17), resultSet.getString(18), resultSet.getString(19), resultSet.getString(20), resultSet.getShort(21), resultSet.getShort(22), resultSet.getString(23), resultSet.getString(24), resultSet.getString(25), resultSet.getString(26), resultSet.getString(27),resultSet.getInt(28), resultSet.getString(29));
    	  			    	CustomerDto CustomerDto = new CustomerDto();
     				       
    				    	CustomerDto.setCustomer_id(resultSet.getInt(1));
    				    	CustomerDto.setAccount_num(resultSet.getDouble(2));
    				    	CustomerDto.setLname(resultSet.getString(3));
    				    	CustomerDto.setFname(resultSet.getString(4));
    				    	CustomerDto.setMi(resultSet.getString(5));
    				    	CustomerDto.setAddress1(resultSet.getString(6));
    				    	CustomerDto.setAddress2(resultSet.getString(7));
    				    	CustomerDto.setAddress3(resultSet.getString(8));
    				    	CustomerDto.setAddress4(resultSet.getString(9));
    				    	CustomerDto.setCity(resultSet.getString(10));
    				    	CustomerDto.setState_province(resultSet.getString(11));
    				    	CustomerDto.setPostal_code(resultSet.getString(12));
    				    	CustomerDto.setCountry(resultSet.getString(13));
    				    	CustomerDto.setCustomer_region_id(resultSet.getString(14));
    				    	CustomerDto.setPhone1(resultSet.getString(15));
    				    	CustomerDto.setPhone2(resultSet.getString(16));
    				    	CustomerDto.setBirthdate(resultSet.getString(17));
    				    	CustomerDto.setMarital_status(resultSet.getString(18));
    				    	CustomerDto.setYearly_income(resultSet.getString(19));
    				    	CustomerDto.setGender(resultSet.getString(20));
    				    	CustomerDto.setTotal_children(resultSet.getShort(21));
    				    	CustomerDto.setNum_children_at_home(resultSet.getShort(22));
    				    	CustomerDto.setEducation(resultSet.getString(23));
    				    	CustomerDto.setDate_accnt_opened(resultSet.getString(24));
    				    	CustomerDto.setMember_card(resultSet.getString(25));
    				    	CustomerDto.setOccupation(resultSet.getString(26));
    				    	CustomerDto.setHouseowner(resultSet.getString(27));
    				    	CustomerDto.setNum_cars_owned(resultSet.getInt(28));
    				    	CustomerDto.setFullname(resultSet.getString(29));
    				    	
    				        CustomerDtoList.add(CustomerDto);
    	  			      }
    	  			      resultSet.close();
    	  			      statement.close();
    	  			      connection.close();    	  			      
    	  			      csvPrinter.flush();
    			  		    			  			    
    			  		 String line = "abc";
    			  		   BufferedReader br = new BufferedReader(new FileReader(CUSTOMER_CSV_FILE));
    			  		 try {
    			  		      	StringBuilder sb = new StringBuilder();
    			  		      	line = br.readLine();

    			  			      	while (line != null) {
    			  			            sb.append(line);
    			  			            sb.append("\n");
    			  			            line = br.readLine();
    			  			        }
    			  			        return sb.toString();
    			  		 } finally {
    			  			  	br.close();
    			  		 }    			  			    
    
    	  			    } catch (SQLException e) {
    	  			      e.printStackTrace();
    	  			      return "SQLException in class DataController : " + e.getMessage();
    	  			    }
    		    }   	    
    	    
}
